import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Content extends ChangeNotifier {
  String url = 'https://fluttertrainingsl-default-rtdb.firebaseio.com';
  List<ContentModel> _items = [
    ContentModel(
      id: 'p1',
      title: 'Red Shirt',
      image: 'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    ),
    ContentModel(
      id: 'p2',
      title: 'Trousers',
      image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
    ),
    ContentModel(
      id: 'p3',
      title: 'Yellow Scarf',
      image: 'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
    ),
    ContentModel(
      id: 'p4',
      title: 'A Pan',
      image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
    ),
  ];

  List<ContentModel> get items {
    return _items;
  }

  Future<void> addItems(ContentModel item) async {
    try {
      Map<String, String> bodyItem = {'id': item.id, 'title': item.title, 'image': item.image};
      var jsonBody = convert.jsonEncode(bodyItem);
      var response = await http.post('$url/contents.json', body: jsonBody);
      print(response);
      _items.add(item);
      notifyListeners();
    } catch (err) {
      print(err);
    }
  }

  Future<void> getContentItems() async {
    List<ContentModel> contentItems = [];
    var response = await http.get('$url/contents.json');
    try {
      if (response.statusCode == 200) {
        var resBody = convert.jsonDecode(response.body) as Map<String, dynamic>;
        resBody.forEach((id, value) {
          contentItems.add(ContentModel(
            id: id,
            title: value['title'],
            image: value['image'],
          ));
        });
        _items = contentItems;
        notifyListeners();
      }
    } catch (err) {
      print(err);
    }
  }
}

class ContentModel {
  final String id;
  final String title;
  final String image;

  ContentModel({
    this.id,
    this.title,
    this.image,
  });
}
