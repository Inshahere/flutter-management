import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:session_flutter/model/content.dart';
import 'package:session_flutter/screens/content_add.dart';
import 'package:session_flutter/widgets/content_item.dart';

class ContentList extends StatefulWidget {
  const ContentList({Key key}) : super(key: key);

  @override
  _ContentListState createState() => _ContentListState();
}

class _ContentListState extends State<ContentList> {
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      isLoading = true;
    });
    Future.delayed(Duration(seconds: 0), () {
      Provider.of<Content>(context, listen: false).getContentItems().then((_) {
        setState(() {
          isLoading = false;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contents'),
      ),
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.white,
        child: Icon(Icons.add),
        onPressed: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ContentAdd(),
        )),
      ),
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : Consumer<Content>(
              builder: (context, content, child) {
                return ListView.builder(
                  itemCount: content.items.length,
                  itemBuilder: (context, index) => ContentListItem(
                    content.items[index],
                  ),
                );
              },
            ),
    );
  }
}
