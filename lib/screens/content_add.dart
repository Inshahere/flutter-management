import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:session_flutter/model/content.dart';

class ContentAdd extends StatefulWidget {
  ContentAdd({Key key}) : super(key: key);

  @override
  _ContentAddState createState() => _ContentAddState();
}

class _ContentAddState extends State<ContentAdd> {
  final formkey = GlobalKey<FormState>();
  String id;
  String title;
  String image;

  void formSubmit() {
    ContentModel item = ContentModel(id: id, title: title, image: image);
    Provider.of<Content>(context, listen: false).addItems(item);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Content'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Center(
          child: Form(
            child: Column(children: [
              TextFormField(
                decoration: InputDecoration(labelText: 'Id'),
                onChanged: (value) {
                  id = value;
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Title'),
                onChanged: (value) {
                  title = value;
                },
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Image'),
                onChanged: (value) {
                  image = value;
                },
              ),
              RaisedButton(onPressed: formSubmit, child: Text('Submit'))
            ]),
          ),
        ),
      ),
    );
  }
}
