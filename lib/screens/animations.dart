import 'dart:math';

import 'package:flutter/material.dart';
import 'package:session_flutter/screens/nativeFeature.dart';

class AnimationSection extends StatefulWidget {
  AnimationSection({Key key}) : super(key: key);

  @override
  _AnimationSectionState createState() => _AnimationSectionState();
}

class _AnimationSectionState extends State<AnimationSection> {
  Color color;
  double borderRadius;
  double margin;
  Duration _duration = Duration(milliseconds: 400);
  double opacityLevel = 0.0;

  @override
  void initState() {
    super.initState();
    color = Colors.deepPurple;
    borderRadius = Random().nextDouble() * 64;
    margin = Random().nextDouble() * 64;
  }

  void change() {
    Color tempColor;
    if (color == Colors.yellowAccent) {
      tempColor = Colors.deepPurple;
    } else {
      tempColor = Colors.yellowAccent;
    }
    setState(() {
      color = tempColor;
      borderRadius = Random().nextDouble() * 64;
      margin = Random().nextDouble() * 64;
    });
  }

  void _gotoNativeScreen() {
    Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => NativeFeature()));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animation Section'),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.orangeAccent, width: 5),
              ),
              child: Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      width: 128,
                      height: 128,
                      child: AnimatedContainer(
                        margin: EdgeInsets.all(margin),
                        decoration: BoxDecoration(
                          color: color,
                          borderRadius: BorderRadius.circular(borderRadius),
                        ),
                        duration: _duration,
                      ),
                    ),
                    RaisedButton(
                      child: Text('Change Shape'),
                      onPressed: () => change(),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.greenAccent, width: 5),
              ),
              child: Center(
                child: Column(
                  children: <Widget>[
                    RaisedButton(
                        child: Text(
                          'Show Fade Effect',
                        ),
                        onPressed: () {
                          if (opacityLevel == 1.0) {
                            setState(() {
                              opacityLevel = 0.0;
                            });
                          } else {
                            setState(() {
                              opacityLevel = 1.0;
                            });
                          }
                        }
                        // setState(() {
                        //   opacityLevel == 1.0 ? 0.0 : 1.0;
                        // }),
                        ),
                    AnimatedOpacity(
                      duration: Duration(seconds: 3),
                      opacity: opacityLevel,
                      child: Column(
                        children: <Widget>[
                          Text('Type: Implicit'),
                          Text('Flutter SDK'),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            RaisedButton.icon(onPressed: _gotoNativeScreen, icon: Icon(Icons.forward), label: Text('Check this'))
          ],
        ),
      ),
    );
  }
}
