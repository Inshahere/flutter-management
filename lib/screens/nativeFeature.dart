import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
// import 'package:path_provider/path_provider.dart' as sysPaths;
// import 'package:image_picker/image_picker.dart';
import 'dart:io';

class NativeFeature extends StatefulWidget {
  const NativeFeature({Key key}) : super(key: key);

  @override
  _NativeFeatureState createState() => _NativeFeatureState();
}

class _NativeFeatureState extends State<NativeFeature> {
  File _storeImage;
  bool toShowImage = false;
  File saveImage;

  _addImage() async {
    // final picker = ImagePicker();
    // final imageSource = await picker.getImage(
    //   source: ImageSource.camera,
    //   maxWidth: 600,
    // );
    // if (imageSource == null) {
    //   return;
    // }
    // setState(() {
    //   _storeImage = File(imageSource.path);
    // });
    // final appDir = await sysPaths.getApplicationDocumentsDirectory();
    // final fileName = path.basename(imageSource.path);
    // saveImage = await _storeImage.copy('${appDir.path}/$fileName');
    // setState(() {
    //   toShowImage = true;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Native Feature'),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  RaisedButton.icon(onPressed: _addImage, icon: Icon(Icons.camera_alt), label: Text('Take Picture')),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      height: MediaQuery.of(context).size.height * 0.35,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 5),
                      ),
                      child: toShowImage
                          ? Image.file(
                              saveImage,
                              fit: BoxFit.cover,
                            )
                          : Container())
                ],
              ),
            ),
          ),
        ));
  }
}
