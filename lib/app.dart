import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:session_flutter/screens/content_list.dart';

import 'model/content.dart';
import 'screens/animations.dart';

class MyApp extends StatelessWidget {
  MyApp();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: Content(),
      child: MaterialApp(
        home: ContentList(),
        //AnimationSection(),
      ),
    );
  }
}
