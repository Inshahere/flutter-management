import 'package:flutter/material.dart';
import 'package:session_flutter/model/content.dart';

class ContentListItem extends StatelessWidget {
  final ContentModel item;
  const ContentListItem(this.item);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTile(
        leading: CircleAvatar(
          child: Image.network(item.image),
        ),
        title: Text(item.title),
      ),
    );
  }
}
